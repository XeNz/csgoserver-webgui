import * as React from "react";
import { RouteComponentProps } from "react-router";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";

interface LastActionState {
    lastAction: string;
}

export class Home extends React.Component<RouteComponentProps<{}>, LastActionState> {
    API_URL: string = "http://localhost:5050/api/";

    constructor() {
        super();
        this.state = { lastAction: "None" };
    }

    render() {
        return <div>
                   <div className="panel-row row">
                       <div className="col s12 m12 l12">
                           <div className="card blue-grey darken-1">
                               <div className="card-content white-text">
                                   <span className="card-title">Server actions</span>
                                   <p>
                                       Choose an action to perform on the CS:GO server.
                                   </p>
                                   <p>

                                       Last action used: <strong>{this.state.lastAction}</strong>
                                   </p>

                               </div>
                               <div className="card-action">
                                   <a href="#" onClick={() => { this.updateServer() }}>Update</a>
                                   <a href="#" onClick={() => { this.startServer() }}>Start</a>
                                   <a href="#" onClick={() => { this.stopServer() }}>Stop</a>
                               </div>
                           </div>
                       </div>
                   </div>
                   <ToastContainer
                       position="bottom-right"
                       autoClose={5000}
                       hideProgressBar={true}
                       newestOnTop={true}
                       closeOnClick
                       pauseOnHover/>
               </div>;
    }

    updateServer() {
        const action: string = "update";
        this.setState({
            lastAction: action
        });
        this.showActionToast(action);
        axios.get("/api/ServerActions/update")
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                this.showResultToast(error);
            });
    }

    startServer() {
        const action: string = "start";
        this.setState({
            lastAction: action
        });
        this.showActionToast(action);
        return axios.get("/api/ServerActions/start")
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                this.showResultToast(error);
            });
    }

    stopServer() {
        const action: string = "stop";
        this.setState({
            lastAction: action
        });
        this.showActionToast(action);
        axios.get("/api/ServerActions/stop")
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                this.showResultToast(error);
            });
    }

    showActionToast(action: string) {
        let verb: string;
        if (action === "stop")
            verb = "Stopping";
        else if (action === "start")
            verb = "Starting";
        else if (action === "update")
            verb = "Updating";
        else
            verb = "not doing anything with";

        toast.info(`Currently ${verb} the server`);
    }

    showResultToast(result: string) {
        toast.info(`${result}`);
    }
}