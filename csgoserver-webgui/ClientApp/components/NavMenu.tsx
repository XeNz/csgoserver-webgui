import * as React from 'react';
import { Link, NavLink } from 'react-router-dom';

export class NavMenu extends React.Component<{}, {}> {
    public render() {
        return <div>
                   <nav>
                       <div className="nav-wrapper">
                           <a href="#" className="brand-logo truncate"> CS:GO server Web GUI</a>
                           <ul id="nav-mobile" className="right hide-on-med-and-down">
                               <Link className='navbar-brand' to={'/'}>Home</Link>
                           </ul>
                       </div>
                   </nav>
        </div>;
    }
}
