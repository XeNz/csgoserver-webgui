﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace csgoserver_webgui.Controllers
{
    [Route("api/[controller]")]
    public class ServerActionsController : Controller
    {
        private const string Directory = "/home/csgoserver/csgoserver";

        [HttpGet("test")]
        public IActionResult Test()
        {
            return new ObjectResult("test");
        }

        [HttpGet("update")]
        public IActionResult UpdateServer()
        {
            var command = Directory + " update";
            var output = command.Bash();
            return new ObjectResult(output);
        }

        [HttpGet("start")]
        public IActionResult StartServer()
        {
            var command = Directory + " start";
            var output = command.Bash();
            return new ObjectResult(output);
        }

        [HttpGet("stop")]
        public IActionResult StopServer()
        {
            var command = Directory + " stop";
            var output = command.Bash();
            return new ObjectResult(output);
        }
    }
}
